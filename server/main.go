package main

import (
	"fmt"
	"gitlab.com/alibitek-go/grpc-blockchain/proto"
	"gitlab.com/alibitek-go/grpc-blockchain/server/blockchain"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	listener, err := net.Listen("tcp", ":7337")
	if err != nil {
		log.Fatalf("unable to listen on port 8080: %v", err)
	}
	fmt.Println("Hello world blockchain server is listening for incoming clients at port 7337!")

	grpcServer := grpc.NewServer()
	proto.RegisterBlockchainServer(grpcServer, &Server {
		Blockchain: blockchain.NewBlockchain(),
	})
	grpcServer.Serve(listener)
}

type Server struct {
	Blockchain *blockchain.Blockchain
}

func (server *Server) AddBlock(context context.Context, request *proto.AddBlockRequest) (*proto.AddBlockResponse, error) {
	block := server.Blockchain.AddBlock(request.Data)
	return &proto.AddBlockResponse{
		Hash: block.Hash,
	}, nil
}

func (server *Server) GetBlockchain(context context.Context, request *proto.GetBlockChainRequest) (*proto.GetBlockchainResponse, error) {
	response := new(proto.GetBlockchainResponse)
	for _, block := range server.Blockchain.Blocks {
		response.Blocks = append(response.Blocks, &proto.Block{
			PreviousBlockHash: block.PreviousBlockHash,
			Hash: block.Hash,
			Data: block.Data,
		})
	}
	return response, nil
}
package blockchain

import (
	"crypto/sha256"
	"encoding/hex"
)

type Block struct {
	Hash              string
	PreviousBlockHash string
	Data              string
}

type Blockchain struct {
	Blocks []*Block
}

func (block *Block) setHash() {
	hash := sha256.Sum256([]byte(block.PreviousBlockHash + block.Data))
	block.Hash = hex.EncodeToString(hash[:])
}

func NewBlock(data string, previousBlockHash string) *Block {
	block := &Block {
		Data:              data,
		PreviousBlockHash: previousBlockHash,
	}
	block.setHash()

	return block
}

func (blockchain *Blockchain) AddBlock(data string) *Block {
	numberOfBlocks := len(blockchain.Blocks)

	// Missing genesis block
	if numberOfBlocks == 0 {
		return nil
	}

	previousBlock := blockchain.Blocks[numberOfBlocks - 1]
	newBlock := NewBlock(data, previousBlock.Hash)
	blockchain.Blocks = append(blockchain.Blocks, newBlock)

	return newBlock
}

func NewBlockchain() *Blockchain {
	return &Blockchain{[]*Block{NewGenesisBlock()}}
}

func NewGenesisBlock() *Block {
	return NewBlock("Genesis block", "")
}
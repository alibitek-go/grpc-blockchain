# grpc-blockchain

Simple [blockchain](https://en.wikipedia.org/wiki/Blockchain) implementation using [gRPC](https://en.wikipedia.org/wiki/GRPC) for client and server communication,   
with protocol buffers as the underlying, transparent, binary wire format. 

## Protocol Buffers
- Download protocol buffer compiler (protoc) for your platform and add it to PATH: https://github.com/protocolbuffers/protobuf/releases
- Install protobuf plugin for Go: `go get -u github.com/golang/protobuf/protoc-gen-go`
- Generate sources: `protoc --go_out=plugins=grpc:. proto/blockchain.proto`

## Dependencies
`go get -u github.com/golang/protobuf/proto google.golang.org/grpc golang.org/x/net/context`

## Run

### Server
`go run server/main.go`  
When successful, the server listens on port 7337 and prints a welcoming message.

### Client

#### Add block to blockchain
`go run client/main.go --addBlock`  
For demonstration purposes, the current timestamp is used as data in a newly created block.  

#### List blocks in blockchain
`go run client/main.go --listBlockchain`

## Install as Go package
```
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
go get gitlab.com/alibitek-go/grpc-blockchain
```

## Usage
- Help
```
go run client/main.go --help
   -addBlock
         Add new block
   -listBlockchain
         Get the blockchain
```
- Cold start (only genesis block present in the blockchain):
```
go run client/main.go --listBlockchain
2019/08/08 19:13:37 Blocks:
2019/08/08 19:13:37 [Block #1] Hash: 4e17811011ec217ac84a9e037a82758a7de318342886a88a37ebabf90f52af73, Previous Block Hash: , Data: Genesis block
```
- Add 5 blocks (genesis block + the other 5 blocks are added to the blockchain):
```
for i in {1..5}; do go run client/main.go --addBlock; done && go run client/main.go --listBlockchain
2019/08/08 19:14:19 New block hash: de99ec922bcc624acdcac97c91dd27751c4c029ea49376f1238fd7cb6655bd35
2019/08/08 19:14:20 New block hash: d980f4104b53184f3ad433b3bec4c9b9e21a0384d5f185b8d04c68b8ab1692b9
2019/08/08 19:14:20 New block hash: c2b754ae25c72fbeb20e2b6b96861741f087e5cc98b13ff22caf35d65c7717ac
2019/08/08 19:14:21 New block hash: 39617fbb1ed4a3c5bd568bc3653512ee2d07f7cbf697f0e9d76f20f5e6fcfe94
2019/08/08 19:14:22 New block hash: caea98571f408b2fc7acbbd70dc8d4ec8daa123f1b56182439aff96bdd306a95
2019/08/08 19:14:22 Blocks:
2019/08/08 19:14:22 [Block #1] Hash: 4e17811011ec217ac84a9e037a82758a7de318342886a88a37ebabf90f52af73, Previous Block Hash: , Data: Genesis block
2019/08/08 19:14:22 [Block #2] Hash: de99ec922bcc624acdcac97c91dd27751c4c029ea49376f1238fd7cb6655bd35, Previous Block Hash: 4e17811011ec217ac84a9e037a82758a7de318342886a88a37ebabf90f52af73, Data: 2019-08-08 19:14:19.632795492 +0300 EEST m=+0.000993744
2019/08/08 19:14:22 [Block #3] Hash: d980f4104b53184f3ad433b3bec4c9b9e21a0384d5f185b8d04c68b8ab1692b9, Previous Block Hash: de99ec922bcc624acdcac97c91dd27751c4c029ea49376f1238fd7cb6655bd35, Data: 2019-08-08 19:14:20.241900383 +0300 EEST m=+0.000928850
2019/08/08 19:14:22 [Block #4] Hash: c2b754ae25c72fbeb20e2b6b96861741f087e5cc98b13ff22caf35d65c7717ac, Previous Block Hash: d980f4104b53184f3ad433b3bec4c9b9e21a0384d5f185b8d04c68b8ab1692b9, Data: 2019-08-08 19:14:20.899344149 +0300 EEST m=+0.000963197
2019/08/08 19:14:22 [Block #5] Hash: 39617fbb1ed4a3c5bd568bc3653512ee2d07f7cbf697f0e9d76f20f5e6fcfe94, Previous Block Hash: c2b754ae25c72fbeb20e2b6b96861741f087e5cc98b13ff22caf35d65c7717ac, Data: 2019-08-08 19:14:21.50430037 +0300 EEST m=+0.000845877
2019/08/08 19:14:22 [Block #6] Hash: caea98571f408b2fc7acbbd70dc8d4ec8daa123f1b56182439aff96bdd306a95, Previous Block Hash: 39617fbb1ed4a3c5bd568bc3653512ee2d07f7cbf697f0e9d76f20f5e6fcfe94, Data: 2019-08-08 19:14:22.121272361 +0300 EEST m=+0.000941999

```

package main

import (
	"flag"
	"gitlab.com/alibitek-go/grpc-blockchain/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"time"
)

func main() {
	addFlag := flag.Bool("addBlock", false, "Add new block")
	listFlag := flag.Bool("listBlockchain", false, "Get the blockchain")
	flag.Parse()

	connection, err := grpc.Dial("localhost:7337", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Cannot dial server: %v", err)
	}

	client := Client{ BlockchainClient: proto.NewBlockchainClient(connection) }

	if *addFlag {
		client.addBlock()
	}

	if *listFlag {
		client.getBlockchain()
	}
}

type Client struct {
	BlockchainClient proto.BlockchainClient
}

func (client *Client) AddBlock(context context.Context, request *proto.AddBlockRequest) (*proto.AddBlockResponse, error) {
	return client.BlockchainClient.AddBlock(context, request)
}

func (client *Client) GetBlockchain(context context.Context,
	request *proto.GetBlockChainRequest) (*proto.GetBlockchainResponse, error) {
	return client.BlockchainClient.GetBlockchain(context, request)
}

func (client *Client) addBlock() {
	block, err := client.AddBlock(context.Background(), &proto.AddBlockRequest{
		Data: time.Now().String(),
	})
	if err != nil {
		log.Fatalf("Unable to add block: %v", err)
	}
	log.Printf("New block hash: %s\n", block.Hash)
}

func (client *Client) getBlockchain() {
	blockchain, err := client.GetBlockchain(context.Background(), &proto.GetBlockChainRequest{})
	if err != nil {
		log.Fatalf("Unable to get blockchain: %v", err)
	}
	log.Println("Blocks:")
	for index, block := range blockchain.Blocks {
		log.Printf("[Block #%d] Hash: %s, Previous Block Hash: %s, Data: %s\n",
			index + 1,
			block.Hash,
			block.PreviousBlockHash,
			block.Data)
	}
}